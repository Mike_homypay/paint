<?php

namespace App\Entity;

use App\Entity\Blogpost;
use App\Entity\Peinture;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\CommentaireRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CommentaireRepository::class)
 */
class Commentaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *  min = 2,
     *  max = 20,
     *  minMessage = "Votre nom doit comporter au moins {{ limit }} caractères",
     *  maxMessage = "Votre nom ne peut pas dépasser {{ limit }} caractères"
     * )
     */
    private $auteur;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *  max = 50,
     *  maxMessage = "Votre email ne peut pas dépasser {{ limit }} caractères"
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *  min = 10,
     *  max = 150,
     *  minMessage = "Votre commentaire doit comporter au moins {{ limit }} caractères",
     *  maxMessage = "Votre nom ne peut pas dépasser {{ limit }} caractères"
     * )
     */
    private $contenu;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Peinture::class, inversedBy="commentaires")
     */
    private $peinture;

    /**
     * @ORM\ManyToOne(targetEntity=Blogpost::class, inversedBy="commentaires")
     */
    private $blogpost;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPeinture(): ?Peinture
    {
        return $this->peinture;
    }

    public function setPeinture(?Peinture $peinture): self
    {
        $this->peinture = $peinture;

        return $this;
    }

    public function getBlogpost(): ?Blogpost
    {
        return $this->blogpost;
    }

    public function setBlogpost(?Blogpost $blogpost): self
    {
        $this->blogpost = $blogpost;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }
}
