<?php

namespace App\Service;

use App\Entity\Contact;
use App\Entity\Blogpost;
use App\Entity\Peinture;
use App\Entity\Commentaire;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class CommentaireService
{
    private $manager;
    private $flash;

    public function __construct(
        EntityManagerInterface $manager,
        FlashBagInterface $flash
    ) {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistCommentaire(
        Commentaire $commentaire,
        Blogpost $blogpost = null,
        Peinture $peinture = null
    ) {
        $commentaire
            ->setCreatedAt(new \DateTimeImmutable('now'))
            ->setIsPublished(false)
            ->setBlogpost($blogpost)
            ->setPeinture($peinture);

        $this->manager->persist($commentaire);
        $this->manager->flush();
        $this->flash->add('success', 'Votre message a bien été envoyé, merci');
    }

    public function isSend(Contact $contact)
    {
        $contact->setIsSend(true);

        $this->manager->persist($contact);
        $this->manager->flush();
    }
}
