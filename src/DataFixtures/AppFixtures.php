<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use DateTimeImmutable;
use App\Entity\Blogpost;
use App\Entity\Peinture;
use App\Entity\Categorie;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $encoder;

    /**
     * Undocumented function
     *
     * @param UserPasswordHasherInterface $encoder
     */
    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    /**
     * Chargement des fixtures
     *
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        //initialisation de Faker
        $faker = Factory::create('fr_FR');

        //creation d'un utilisateur
        $user = new User();

        $user
            ->setEmail('servam95@gmail.com')
            ->setPrenom('Eloïse')
            ->setNom($faker->lastName())
            ->setTelephone($faker->phoneNumber())
            ->setAPropos($faker->text())
            ->setInstagram('instagram')
            ->setRoles(['ROLE_PEINTRE']);

        $password = $this->encoder->hashPassword($user, 'password');

        $user
            ->setPassword($password);

        $manager->persist($user);
        $manager->flush();

        //Creation de 10 Blogpost
        for ($i = 0; $i < 10; $i++) {
            $blogpost = new Blogpost();
            //creation d'une nouvelle date sur l'année en cours / transformation de l'objet en string
            $dateNow = new DateTimeImmutable();
            $datetime_immutable = new DateTimeImmutable($faker->dateTimeThisYear()->format('Y-m-d'));
            $dateRealisation  = new DateTimeImmutable($faker->dateTimeBetween('-3 years', 'now')->format('Y-m-d'));

            $blogpost
                ->setTitre($faker->company())
                ->setContenu($faker->paragraph(15))
                ->setSlug($faker->slug(3))
                ->setCreatedAt($datetime_immutable)
                ->setUser($user);

            $manager->persist($blogpost);
        }

        //creation d'un blogpost pour les tests
        $blogpost = new Blogpost();
        $datetime_immutable = new \DateTimeImmutable();

        $blogpost
            ->setTitre('Blogpost test')
            ->setContenu($faker->paragraph(15))
            ->setSlug('$faker->slug(2)')
            ->setCreatedAt($datetime_immutable)
            ->setUser($user);

        $manager->persist($blogpost);


        $manager->flush();

        //creation de 5 categories
        for ($j = 0; $j < 5; $j++) {
            $categorie = new Categorie();

            $categorie
                ->setNom($faker->word())
                ->setDescription($faker->text())
                ->setSlug($faker->slug(3));

            $manager->persist($categorie);

            //on cree deux peintures par categorie
            for ($k = 0; $k < 3; $k++) {
                $peinture = new Peinture();

                $peinture
                    ->setNom($faker->words(3, true))
                    ->setLargeur($faker->randomFloat(2, 20, 60))
                    ->sethauteur($faker->randomFloat(2, 20, 60))
                    ->setEnVente($faker->randomElement([true, false]))
                    ->setPrix($faker->randomFloat(2, 100, 9999))
                    ->setDateRealisation($dateRealisation)
                    ->setCreatedAt($dateNow)
                    ->setDescription($faker->text())
                    ->setPortfolio($faker->randomElement([true, false]))
                    ->setSlug($faker->slug(3))
                    ->setFile(
                        $faker->randomElement(
                            [
                                'cezanne',
                                'flowers_nemecky',
                                'garden_slovakia',
                                'the_seine',
                                'categorie',
                                'la_tache',
                                'bord_eau',
                                'cellule',
                                'foret_automne',
                                'noir',
                                'venise',
                            ]
                        ) . '.jpg'
                    )
                    ->addCategorie($categorie)
                    ->setUser($user);

                $manager->persist($peinture);
            }
        }
        //on crée une catégorie pour les tests
        $categorie = new Categorie();

        $categorie
            ->setNom('Categorie test')
            ->setDescription($faker->text())
            ->setSlug('categorie-test');

        $manager->persist($categorie);

        $peinture = new Peinture();

        $peinture
            ->setNom('Peinture test')
            ->setLargeur($faker->randomFloat(2, 20, 60))
            ->sethauteur($faker->randomFloat(2, 20, 60))
            ->setEnVente($faker->randomElement([true, false]))
            ->setPrix($faker->randomFloat(2, 100, 9999))
            ->setDateRealisation($dateRealisation)
            ->setCreatedAt($dateNow)
            ->setDescription($faker->text())
            ->setPortfolio($faker->randomElement([true, false]))
            ->setSlug('peinture-test')
            ->setFile(
                $faker->randomElement(
                    [
                        'cezanne',
                        'flowers_nemecky',
                        'garden_slovakia',
                        'the_seine',
                        'categorie',
                        'la_tache',
                        'bord_eau',
                        'cellule',
                        'foret_automne',
                        'noir',
                        'venise',
                    ]
                ) . '.jpg'
            )
            ->addCategorie($categorie)
            ->setUser($user);

        $manager->persist($peinture);
        $manager->flush();
    }
}
