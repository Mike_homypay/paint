<?php

namespace App\Twig;

use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use App\Repository\CategorieRepository;

class CategorieExtension extends AbstractExtension
{
    private $categorieRepository;

    public function __construct(CategorieRepository $categorieRepository)
    {
        $this->categorieRepository = $categorieRepository;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('categorieNavbar', [$this, 'categorie']),
        ];
    }
    /**
     * On récupere toutes les categories sur chacune des pages
     *
     * @return array
     */
    public function categorie(): array
    {
        return
            $this->categorieRepository->findAll();
    }
}
