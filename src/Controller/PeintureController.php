<?php

namespace App\Controller;

use App\Entity\Peinture;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Service\CommentaireService;
use App\Repository\PeintureRepository;
use App\Repository\CommentaireRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Template
 * @Route("/peinture")
 */
class PeintureController extends AbstractController
{
    /**
     * @Route("/realisations")
     * @return Peinture[]
     */
    public function realisations(
        PeintureRepository $peintureRepository,
        PaginatorInterface $paginator,
        Request $request
    ): array {
        $data = $peintureRepository->findAll();
        $peintures = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );
        return [
            'peintures' => $peintures
        ];
    }
    /**
     * @Route("/details/{slug}")
     *
     * @return Response
     */
    public function details(
        Peinture $peinture,
        Request $request,
        CommentaireService $commentaireService,
        ValidatorInterface $validator,
        CommentaireRepository $commentaireRepository
    ) {
        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $commentaire = $form->getData();
            $commentaireService->persistCommentaire($commentaire, null, $peinture);
            return $this->redirectToRoute('app_peinture_details', ['slug' => $peinture->getSlug()]);
        }
        $errors = $validator->validate($commentaire);
        if (count($errors) > 0) {
            /*
             * Uses a __toString method on the $errors variable which is a
             * ConstraintViolationList object. This gives us a nice string
             * for debugging.
             */
            return  [
                'errors' => $errors,
                'peinture' => $peinture,
                'form' => $form->createView(),
                'commentairePeintures' => $commentaireRepository->findByPeinture($peinture)
            ];
        }
        return [
            'peinture' => $peinture,
            'form' => $form->createView(),
            'commentairePeintures' => $commentaireRepository->findByPeinture($peinture)
        ];
    }
}
