<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Template
 * @Route("/apropos")
 */
class AProposController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(UserRepository $userRepository): array
    {
        $peintre = $userRepository->getPeintre();
        return [
            'peintre' => $peintre,
        ];
    }
}
