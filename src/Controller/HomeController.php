<?php

namespace App\Controller;

use App\Repository\BlogpostRepository;
use App\Repository\PeintureRepository;
use App\Repository\UserRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Undocumented class
 * @Template
 * @Route("/")
 */
class HomeController extends AbstractController
{
    /**
     * Page d'accueil
     * @Route("/")
     * @return array
     */
    public function index(
        PeintureRepository $peintureRepository,
        BlogpostRepository $blogpostRepository,
        UserRepository $userRepository
    ) {
        return [
            'peintures' => $peintureRepository->lastThree(),
            'blogposts' => $blogpostRepository->lastThree(),
            'user' => $userRepository->getPeintre()
        ];
    }
}
