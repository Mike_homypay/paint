<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Repository\CategorieRepository;
use App\Repository\PeintureRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Template
 * @Route("/portfolio")
 */
class PortfolioController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(CategorieRepository $categorieRepository): array
    {
        $categories = $categorieRepository->findAll();
        return [
            "categories" => $categories
        ];
    }

    /**
     * @Route("/{slug}")
     *
     * @param PeintureRepository $peintureRepository
     * @param Categorie $categorie
     * @return array
     */
    public function categorie(PeintureRepository $peintureRepository, Categorie $categorie): array
    {
        $peintures = $peintureRepository->findAllPortfolio($categorie);
        return [
            'categorie' => $categorie,
            'peintures' => $peintures,
        ];
    }
}
