<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Service\ContactService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Template
 * @Route("/contact")
 */
class ContactController extends AbstractController
{
    /**
     * @Route("/")
     *
     * @return array
     */
    public function index(Request $request, ContactService $contactService)
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData(); //on récupere dans $contact la data

            $contactService->persistContact($contact);
            return $this->redirectToRoute('app_contact_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
