<?php

namespace App\Controller;

use PhpParser\Node\Stmt\Foreach_;
use App\Repository\BlogpostRepository;
use App\Repository\PeintureRepository;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/sitemap.xml")
 * @Template
 */
class SitemapController extends AbstractController
{
    /**
     * @Route("/", defaults={"_format"="xml"})
     * @return array
     */
    public function index(
        Request $request,
        PeintureRepository $peintureRepository,
        BlogpostRepository $blogpostRepository,
        CategorieRepository $categorieRepository
    ): Response {
        $hostname = $request->getSchemeAndHttpHost();

        $urls = [];
        $urls[] = ['loc' => $this->generateUrl('app_home_index')];
        $urls[] = ['loc' => $this->generateUrl('app_peinture_realisations')];
        $urls[] = ['loc' => $this->generateUrl('app_blogpost_actualites')];
        $urls[] = ['loc' => $this->generateUrl('app_apropos_index')];
        $urls[] = ['loc' => $this->generateUrl('app_portfolio_index')];

        foreach ($peintureRepository->findAll() as $peinture) {
            $urls[] = [
                'loc' => $this->generateUrl('app_peinture_details', ['slug' => $peinture->getSlug()]),
                'lastmod' => $peinture->getCreatedAt()
            ];
        }
        foreach ($blogpostRepository->findAll() as $blogpost) {
            $urls[] = [
                'loc' => $this->generateUrl('app_blogpost_details', ['slug' => $blogpost->getSlug()]),
                'lastmod' => $blogpost->getCreatedAt()
            ];
        }

        foreach ($categorieRepository->findAll() as $categorie) {
            $urls[] = [
                'loc' => $this->generateUrl('app_portfolio_categorie', ['slug' => $categorie->getSlug()])
            ];
        }

        $response = new response(
            $this->renderView('sitemap/index.xml.twig', [
                'urls' => $urls,
                'hostname' => $hostname
            ]),
            200
        );

        $response->headers->set('Content-Type', 'text/xml');
        return
            $response;
    }
}
