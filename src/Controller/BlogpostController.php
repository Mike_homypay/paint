<?php

namespace App\Controller;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Service\ContactService;
use App\Service\CommentaireService;
use App\Repository\BlogpostRepository;
use App\Repository\CommentaireRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

/**
 * @Template
 * @Route("/blogpost")
 */
class BlogpostController extends AbstractController
{
    /**
     * @Route("/actualites")
     */
    public function actualites(
        PaginatorInterface $paginator,
        BlogpostRepository $blogpostRepository,
        Request $request
    ): array {
        $data = $blogpostRepository->findAll();
        $blogposts = $paginator->paginate(
            $data,
            $request->query->get('page', 1),
            6
        );
        return [
            'blogposts' => $blogposts,
        ];
    }

    /**
     * @Route("/details/{slug}")
     * @param Blogpost $blogpost
     * @param Request $request
     * @param CommentaireService $commentaireService
     * @return Response
     */
    public function details(
        Blogpost $blogpost,
        Request $request,
        CommentaireService $commentaireService,
        CommentaireRepository $commentaireRepository,
        ValidatorInterface $validator
    ) {
        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentaire = $form->getData();
            $commentaireService->persistCommentaire($commentaire, $blogpost, null);
            return $this->redirectToRoute('app_blogpost_details', ['slug' => $blogpost->getSlug()]);
        }

        $errors = $validator->validate($commentaire);
        if (count($errors) > 0) {
            /*
             * Uses a __toString method on the $errors variable which is a
             * ConstraintViolationList object. This gives us a nice string
             * for debugging.
             */
            return  [
                'errors' => $errors,
                'blogpost' => $blogpost,
                'form' => $form->createView(),
                'commentaireBlogposts' => $commentaireRepository->findByBlogpost($blogpost)
            ];
        }

        return [
            'blogpost' => $blogpost,
            'form' => $form->createView(),
            'commentaireBlogposts' => $commentaireRepository->findByBlogpost($blogpost)
        ];
    }
}
