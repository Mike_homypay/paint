<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'create-user';
    protected static $defaultDescription = 'Cette commande de creer un nouvel utilisateur';

    private $entityManagerInterface;
    private $hash;

    public function __construct(
        EntityManagerInterface $entityManagerInterface,
        UserPasswordHasherInterface $hash
    ) {
        $this->entityManagerInterface = $entityManagerInterface;
        $this->hash = $hash;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('username', InputArgument::REQUIRED, 'Le nom de l\'utilisateur')
            ->addArgument('password', InputArgument::REQUIRED, 'Le mot de passe de l\'utilisateur');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $user = new User();

        $password = $this->hash->hashPassword($user, $input->getArgument('password'));

        $user
            ->setPassword($password)
            ->setEmail($input->getArgument('username'))
            ->setRoles(['ROLE_PEINTRE'])
            ->setTelephone('')
            ->setNom('')
            ->setPrenom('')
            ->setAPropos('')
            ->setInstagram('');

            dump($user);
        $this->entityManagerInterface->persist($user);
        $this->entityManagerInterface->flush();

        return Command::SUCCESS;
    }
}
