# Paint

Un site d'exposition et de vente de peinture

## environment

### Configuration

- PHP 7.4
- Composer
- Symfony CLI
- Docker
- Docker-compose
- nodejs et npm

#### Pour controler son environnement

```bash
symfony check:requirements
```

### Lancement de l'environnement de developpement

```bash
composer install
yarn add
yarn encore dev --watch
docker-compose up -d
symfony serve -d
```

### Commandes utiles

Recuperation des droits sur les fichiers

```bash
sudo chown -R 1000:1000 *
```
## Lancer des tests

```bash
php bin/phpunit --testdox
php bin/phpunit --coverage-html var/log/test/test-coverage  #creation d'un fichier de test-coverage dans le dossier var / rapport de tous les tests du site
APP_ENV=test php bin/console d:d:c # creation d'une bdd pour nos tests uniquement
 APP_ENV=test php bin/console d:m:m # migration pour nos tests
  APP_ENV=test php bin/console d:f:l # charge nos fixtures dans la bdd de test
```

## Fixtures

## Production

### Recevoir les messages des visiteurs

Les mails sont stockés en bdd, pour les envoyer au peintre par mail, mise en place d'un cron

```bash
php bin/console send-contact
```

https://packagist.org/packages/fakerphp/faker

```bash
composer require fakerphp/faker
php bin/console doctrine:fixtures:load
```

## Easyadming

### Upload files

https://github.com/dustin10/VichUploaderBundle

```bash
composer require vich/uploader-bundle
```
Bien mapé le lien d'enregistrement des fichiers uploadés dans "service.yaml"

## phpc

https://github.com/squizlabs/PHP_CodeSniffer

```bash
phpcs -v --standard=PSR12 --ignore=./src/Kernel.php ./src 
phpcbf -v --standard=PSR12 --ignore=./src/Kernel.php ./src

## Librairie javascriipt

https://miketricking.github.io/bootstrap-image-hover/
https://ciar4n-izmir.netlify.app/#/

## Template Design

https://bootswatch.com/cerulean/
