import AOS from "aos";
import "aos/dist/aos.css";
AOS.init();
import "./styles/app.scss";

import "./styles/izmir.min.css";

// start the Stimulus application
import "./bootstrap";

import "@fortawesome/fontawesome-free/js/all";

//import local javascript
import "./scripts/navbar";
