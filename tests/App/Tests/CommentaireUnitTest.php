<?php

namespace App\Tests\App\Tests;

use DateTimeImmutable;
use App\Entity\Blogpost;
use App\Entity\Peinture;
use App\Entity\Commentaire;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $commentaire = new Commentaire();
        $datetime_immutable = new DateTimeImmutable();
        $peinture = new Peinture();
        $blogpost = new Blogpost();
        $commentaire
            ->setAuteur('auteur')
            ->setEmail('auteur@example.com')
            ->setContenu('contenu')
            ->setCreatedAt($datetime_immutable)
            ->setPeinture($peinture)
            ->setBlogpost($blogpost);

        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getEmail() === 'auteur@example.com');
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getCreatedAt() === $datetime_immutable);
        $this->assertTrue($commentaire->getPeinture() === $peinture);
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
    }

    public function testIsFalse()
    {
        $commentaire = new Commentaire();
        $datetime_immutable = new DateTimeImmutable();
        $peinture = new Peinture();
        $blogpost = new Blogpost();

        $commentaire
            ->setAuteur('auteur')
            ->setEmail('auteur@example.com')
            ->setContenu('contenu')
            ->setCreatedAt($datetime_immutable)
            ->setPeinture($peinture)
            ->setBlogpost($blogpost);

        $this->assertFalse($commentaire->getAuteur() === 'false');
        $this->assertFalse($commentaire->getEmail() === 'false@example.com');
        $this->assertFalse($commentaire->getContenu() === 'false');
        $this->assertFalse($commentaire->getCreatedAt() === new DateTimeImmutable());
        $this->assertFalse($commentaire->getPeinture() ===  new Peinture());
        $this->assertFalse($commentaire->getBlogpost() === new Blogpost());
    }

    public function testIsEmpty() 
    {
        $commentaire = new Commentaire();

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getPeinture());
        $this->assertEmpty($commentaire->getBlogpost());

    }
}
