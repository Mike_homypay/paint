<?php

use App\Entity\User;
use App\Entity\Blogpost;
use App\Entity\Peinture;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;


class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();
        $user->setEmail('test@example.com')
            ->setPrenom('prenom')
            ->setNom('nom')
            ->setPassword('password')
            ->setAPropos('apropos')
            ->setTelephone('0644005872')
            ->setInstagram('insta')
            ->setRoles(['ROLE_TEST']);

        $this->assertTrue($user->getEmail() === 'test@example.com');
        $this->assertTrue($user->getPrenom() === 'prenom');
        $this->assertTrue($user->getNom() === 'nom');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getAPropos() === 'apropos');
        $this->assertTrue($user->getTelephone() === '0644005872');
        $this->assertTrue($user->getInstagram() === 'insta');
        $this->assertTrue($user->getRoles() === ['ROLE_TEST', 'ROLE_USER']);
    }
    public function testIsFalse()
    {
        $user = new User();
        $user->setEmail('test@example.com')
        ->setPrenom('prenom')
        ->setNom('nom')
        ->setPassword('password')
        ->setAPropos('apropos')
        ->setTelephone('0644005872')
        ->setInstagram('insta')
        ->setRoles(['ROLE_TEST']);


        $this->assertFalse($user->getEmail() === 'false@example.com');
        $this->assertFalse($user->getPrenom() === 'false');
        $this->assertFalse($user->getNom() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getAPropos() === 'false');
        $this->assertFalse($user->getTelephone() === '125015255');
        $this->assertFalse($user->getInstagram() === 'false');
        $this->assertFalse($user->getRoles() === ['ROLE_FALSE', 'ROLE_USER']);
    }
    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getUserName());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        //$this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getTelephone());
        $this->assertEmpty($user->getInstagram());
        $this->assertEmpty($user->getId());
    }

    public function testAddGetRemovePeinture(){
        $user = new User();
        $peinture = new Peinture();

        $this->assertEmpty($user->getPeintures());

        $user->addPeinture($peinture);
        $this->assertContains($peinture, $user->getPeintures());

        $user->removePeinture($peinture);
        $this->assertEmpty($user->getPeintures());
    }

    public function testAddGetRemoveBlogpost(){
        $user = new User();
        $blogpost = new Blogpost();

        $this->assertEmpty($user->getPeintures());

        $user->addBlogpost($blogpost);
        $this->assertContains($blogpost, $user->getBlogposts());

        $user->removeBlogpost($blogpost);
        $this->assertEmpty($user->getBlogposts());
    }
}
