<?php

namespace App\Tests\App\Tests;

use DateTimeImmutable;
use App\Entity\Contact;
use PHPUnit\Framework\TestCase;

class ContactUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $date = new DateTimeImmutable();
        $contact = new Contact();
        $contact
            ->setNom('nom')
            ->setEmail('test@mail.com')
            ->setMessage('ceci est un message d\'utilisateur')
            ->setCreatedAt($date)
            ->setIsSend(true);

        $this->assertTrue($contact->getNom() === 'nom');
        $this->assertTrue($contact->getEmail() === 'test@mail.com');
        $this->assertTrue($contact->getMessage() === 'ceci est un message d\'utilisateur');
        $this->assertTrue($contact->getCreatedAt() === $date);
        $this->assertTrue($contact->getIsSend() === true);
    }
    public function testIsFalse()
    {
        $contact = new Contact();
        $date = new DateTimeImmutable();
        $dateFalsetime = new DateTimeImmutable();
        $contact
            ->setNom('nom')
            ->setEmail('test@mail.com')
            ->setMessage('ceci est un message d\'utilisateur')
            ->setCreatedAt($date)
            ->setIsSend(true);

        $this->assertFalse($contact->getNom() === 'false');
        $this->assertFalse($contact->getEmail() === 'false');
        $this->assertFalse($contact->getMessage() === 'false');
        $this->assertFalse($contact->getCreatedAt() === $dateFalsetime);
        $this->assertFalse($contact->getIsSend() === false);
    }

    public function testIsEmpty()
    { 
        $contact = new Contact();
        
        $this->assertEmpty($contact->getNom());
        $this->assertEmpty($contact->getEmail());
        $this->assertEmpty($contact->getMessage());
        $this->assertEmpty($contact->getCreatedAt());
        $this->assertEmpty($contact->getId());

    }

    
    
}
