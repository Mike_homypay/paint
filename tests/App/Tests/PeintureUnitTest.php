<?php

namespace App\Tests\App\Tests;

use App\Entity\Categorie;
use App\Entity\Commentaire;
use App\Entity\User;
use DateTimeImmutable;
use App\Entity\Peinture;
use PHPUnit\Framework\TestCase;

class PeintureUnitTest extends TestCase
{
    /**
     * Undocumented function
     *
     * @return void
     */
    public function testIsTrue(): void
    {
        $peinture = new Peinture();
        $datetime_immutable = new DateTimeImmutable();
        $user = new User();
        $categorie = new Categorie();
        $peinture
            ->setNom('nom')
            ->setLargeur(16.50)
            ->setHauteur(16.50)
            ->setEnVente(true)
            ->setPrix(550.25)
            ->setDateRealisation($datetime_immutable)
            ->setCreatedAt($datetime_immutable)
            ->setDescription('description')
            ->setPortfolio(true)
            ->setSlug('slug')
            ->setFile('file')
            ->addCategorie($categorie)
            ->setUser($user);

        $this->assertTrue($peinture->getNom() === 'nom');
        $this->assertTrue($peinture->getLargeur() == 16.50);
        $this->assertTrue($peinture->getHauteur() == 16.50);
        $this->assertTrue($peinture->getEnVente() === true);
        $this->assertTrue($peinture->getPrix() == 550.25);
        $this->assertTrue($peinture->getDateRealisation() === $datetime_immutable);
        $this->assertTrue($peinture->getCreatedAt() === $datetime_immutable);
        $this->assertTrue($peinture->getDescription() === 'description');
        $this->assertTrue($peinture->getPortfolio() === true);
        $this->assertTrue($peinture->getSlug() === 'slug');
        $this->assertTrue($peinture->getFile() === 'file');
        $this->assertContains($categorie, $peinture->getCategorie());
        $this->assertTrue($peinture->getUser() === $user);
    }
    /**
     * Undocumented function
     *
     * @return void
     */
    public function testIsFalse(): void
    {
        $peinture = new Peinture();
        $datetime_immutable = new DateTimeImmutable();
        $user = new User();
        $categorie = new Categorie();

        $peinture
            ->setNom('nom')
            ->setLargeur('16.50')
            ->setHauteur('16.50')
            ->setEnVente(true)
            ->setPrix(550.25)
            ->setDateRealisation($datetime_immutable)
            ->setCreatedAt($datetime_immutable)
            ->setDescription('description')
            ->setPortfolio(true)
            ->setSlug('slug')
            ->setFile('file')
            ->addCategorie($categorie)
            ->setUser($user);

        $this->assertFalse($peinture->getNom() === 'false');
        $this->assertFalse($peinture->getLargeur() === '21.54');
        $this->assertFalse($peinture->getHauteur() === '45.74');
        $this->assertFalse($peinture->getEnVente() === false);
        $this->assertFalse($peinture->getPrix() === '45.57');
        $this->assertFalse($peinture->getDateRealisation() === new DateTimeImmutable());
        $this->assertFalse($peinture->getCreatedAt() === new DateTimeImmutable());
        $this->assertFalse($peinture->getDescription() === 'false');
        $this->assertFalse($peinture->getPortfolio() === false);
        $this->assertFalse($peinture->getSlug() === 'false');
        $this->assertFalse($peinture->getFile() === 'false');
        $this->assertNotContains(new Categorie(), $peinture->getCategorie());
        $this->assertFalse($peinture->getUser() === new User());
    }
    
    /**
     * Undocumented function
     *
     * @return void
     */
    public function testIsEmpty()
    {
        $peinture = new Peinture();

        $this->assertEmpty($peinture->getNom());
        $this->assertEmpty($peinture->getLargeur());
        $this->assertEmpty($peinture->getHauteur());
        $this->assertEmpty($peinture->getEnVente());
        $this->assertEmpty($peinture->getPrix());
        $this->assertEmpty($peinture->getDateRealisation());
        $this->assertEmpty($peinture->getCreatedAt());
        $this->assertEmpty($peinture->getDescription());
        $this->assertEmpty($peinture->getPortfolio());
        $this->assertEmpty($peinture->getSlug());
        $this->assertEmpty($peinture->getFile());
        $this->assertEmpty($peinture->getCategorie());
        $this->assertEmpty($peinture->getUser());
        $this->assertEmpty($peinture->getId());
    }

    public function testAddGetRemovePeinture()
    {
        $peinture = new Peinture();
        $commentaire = new Commentaire();

        $this->assertEmpty($peinture->getCommentaires());   

        $peinture->addCommentaire($commentaire);
        $this->assertContains($commentaire, $peinture->getCommentaires());

        $peinture->removeCommentaire($commentaire);
        $this->assertEmpty($peinture->getCommentaires());
    }
}
