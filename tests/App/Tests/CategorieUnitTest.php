<?php

namespace App\Tests\App\Tests;

use App\Entity\Peinture;
use App\Entity\Categorie;
use PHPUnit\Framework\TestCase;

class CategorieUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $categorie = new Categorie();
        $categorie
            ->setNom('nom')
            ->setDescription('Description')
            ->setSlug('apropos');

        $this->assertTrue($categorie->getNom() === 'nom');
        $this->assertTrue($categorie->getSlug() === 'apropos');
        $this->assertTrue($categorie->getDescription() === 'Description');
    }
    public function testIsFalse()
    {
        $categorie = new Categorie();
        $categorie
            ->setNom('nom')
            ->setDescription('Description')
            ->setSlug('apropos');

        $this->assertFalse($categorie->getNom() === 'false');
        $this->assertFalse($categorie->getSlug() === 'false');
        $this->assertFalse($categorie->getDescription() === 'false');
    }
    public function testIsEmpty()
    {
        $categorie = new Categorie();
        
        $this->assertEmpty($categorie->getNom());
        $this->assertEmpty($categorie->getSlug());
        $this->assertEmpty($categorie->getDescription());
        $this->assertEmpty($categorie->getId());

    }

    public function testAddGetRemovePeinture()
    {
        $categorie = new Categorie();
        $peinture = new Peinture();

        $this->assertEmpty($categorie->getPeintures());   

        $categorie->addPeinture($peinture);
        $this->assertContains($peinture, $categorie->getPeintures());

        $categorie->removePeinture($peinture);
        $this->assertEmpty($categorie->getPeintures());
    }
}
